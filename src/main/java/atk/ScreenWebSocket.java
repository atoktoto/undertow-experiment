package atk;

import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import org.xnio.ChainedChannelListener;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;


public class ScreenWebSocket implements WebSocketConnectionCallback {

    private volatile BufferedImage lastImage;

    public BufferedImage takeScreenShot(int width, int height) throws AWTException {
        return new Robot().createScreenCapture(new Rectangle(width, height));
    }


    @Override
    public void onConnect(WebSocketHttpExchange exchange, WebSocketChannel channel) {

        System.out.println("client connected");

        channel.getWorker().execute(() -> {
            while (channel.isOpen()) {
                try {
                    BufferedImage nextImage = takeScreenShot(800, 800);

                    lastImage = nextImage;
                    Thread.sleep(100);
                } catch (AWTException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("close");
        });

        channel.getWorker().execute(() -> {

            ByteBuffer buffer = ByteBuffer.allocate(500 * 1024);

            while (channel.isOpen()) {
                if(lastImage != null) {
                    try {
                        BufferedImage image = lastImage;
                        ByteBufferBackedOutputStream os = new ByteBufferBackedOutputStream(buffer);
                        ImageIO.write(image, "png", os);
                        buffer.flip();
                        WebSockets.sendBinaryBlocking(buffer, channel);
                        buffer.clear();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    static class ByteBufferBackedOutputStream extends OutputStream {
        ByteBuffer buf;

        ByteBufferBackedOutputStream(ByteBuffer buf) {
            this.buf = buf;
        }

        public synchronized void write(int b) throws IOException {
            buf.put((byte) b);
        }

        public synchronized void write(byte[] bytes, int off, int len) throws IOException {
            buf.put(bytes, off, len);
        }

    }

}
