package atk.handlers;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.google.common.io.ByteStreams;
import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * S3Handler can be used to serve files of S3 bucket.
 * Serving file will cause
 */
public class S3Handler implements HttpHandler {

    final AmazonS3 s3Client = new AmazonS3Client();
    final String bucketName;
    final boolean useRedirect;

    public S3Handler(String bucketName, boolean useRedirect) {
        this.bucketName = bucketName;
        this.useRedirect = useRedirect;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        final String item = exchange.getRelativePath().substring(1);
        if(useRedirect) {
            String signedUrl = getSignedUrl(item, 1000 * 60);
            Handlers.redirect(signedUrl).handleRequest(exchange);
        } else {
            streamObject(exchange, bucketName, item);
        }
    }

    private void streamObject(HttpServerExchange exchange, String bucketName, String item) {
        exchange.dispatch(() -> {
            exchange.startBlocking();

            try {
                S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, item));
                ObjectMetadata metadata = object.getObjectMetadata();
                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "" + metadata.getContentType());
                exchange.getResponseHeaders().put(Headers.CONTENT_LENGTH, "" + metadata.getContentLength());
                InputStream objectData = object.getObjectContent();
                OutputStream responseStream = exchange.getOutputStream();

                try {
                    ByteStreams.copy(objectData, responseStream);
                    exchange.endExchange();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (AmazonS3Exception e) {
                exchange.setResponseCode(404);
                exchange.getResponseSender().send("Object not found"); //TODO make it nicer
                exchange.endExchange();
            }
        });
    }

    private String getSignedUrl(String key, long validity) {
        java.util.Date expiration = new java.util.Date();
        long msec = expiration.getTime() + validity;
        expiration.setTime(msec);

        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, key);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
        generatePresignedUrlRequest.setExpiration(expiration);

        URL s = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return s.toString();
    }
}
