package atk.rest;

import atk.handlers.RestHandler;
import atk.model.User;
import io.undertow.server.HttpServerExchange;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserRestHandler extends RestHandler {

    final DataSource db;

    public UserRestHandler(DataSource db) {
        this.db = db;
    }

    @Override
    public void post(HttpServerExchange exchange) throws Exception {
        User user = readObjectFromRequestBody(exchange, User.class);

        try (Connection connection = db.getConnection();
             PreparedStatement q = connection.prepareStatement("INSERT INTO users VALUES (default, ?, ?, ?)")
        ) {
            q.setString(1, user.name);
            q.setString(2, user.email);
            q.setBoolean(3, user.active);
            q.executeUpdate();
        }
    }

    @Override
    public void get(HttpServerExchange exchange) throws Exception {
        List<User> students = new ArrayList<>();

        try (Connection connection = db.getConnection();
             PreparedStatement query = connection.prepareStatement(
                     "SELECT * FROM users LIMIT 10",
                     ResultSet.TYPE_FORWARD_ONLY,
                     ResultSet.CONCUR_READ_ONLY))
        {
            try (ResultSet resultSet = query.executeQuery()) {
                while(resultSet.next()) {
                    students.add(new User(
                            resultSet.getInt("id"),
                            resultSet.getString("name"),
                            resultSet.getString("email"),
                            resultSet.getBoolean("active")
                    ));
                }
            }
        }

        sendResource(exchange, "userList", students);
    }


}
