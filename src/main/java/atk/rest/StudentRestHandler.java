package atk.rest;

import atk.handlers.RestHandler;
import atk.model.Student;
import atk.utils.JsonSender;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.undertow.server.HttpServerExchange;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class StudentRestHandler extends RestHandler {

    final DataSource db;

    public StudentRestHandler(DataSource db) {
        this.db = db;
    }

    @Override
    public void get(HttpServerExchange exchange) throws Exception {
        List<Student> students = new ArrayList<Student>();

        try (Connection connection = db.getConnection();
             PreparedStatement query = connection.prepareStatement(
                     "SELECT * FROM Student LIMIT 10",
                     ResultSet.TYPE_FORWARD_ONLY,
                     ResultSet.CONCUR_READ_ONLY))
        {
            try (ResultSet resultSet = query.executeQuery()) {
                while(resultSet.next()) {
                    students.add(new Student(
                            resultSet.getInt("id"),
                            resultSet.getString("name")));
                }
            }
        }

        sendResource(exchange, "student_list", students);
    }

    @Override
    public void get(HttpServerExchange exchange, String elementId) throws SQLException, JsonProcessingException {
        List<Object> students = new ArrayList<Object>();

        try (Connection connection = db.getConnection();
             PreparedStatement query = connection.prepareStatement(
                     "SELECT * FROM Student WHERE id=? LIMIT 10",
                     ResultSet.TYPE_FORWARD_ONLY,
                     ResultSet.CONCUR_READ_ONLY))
        {
            query.setInt(1, Integer.parseInt(elementId));
            try (ResultSet resultSet = query.executeQuery()) {
                while(resultSet.next()) {
                    students.add(new Student(
                            resultSet.getInt("id"),
                            resultSet.getString("name")));
                }
            }
            sendResource(exchange, "student", students);
        }

    }

    @Override
    public void delete(HttpServerExchange exchange) throws Exception {
        throw new RuntimeException("Test exception");
    }
}
