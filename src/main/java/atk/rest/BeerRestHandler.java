package atk.rest;

import atk.handlers.RestHandler;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import io.undertow.server.HttpServerExchange;


public class BeerRestHandler extends RestHandler {

    final Bucket bucket;

    public BeerRestHandler(Bucket bucket) {
        this.bucket = bucket;
    }

    @Override
    public void get(HttpServerExchange exchange) throws Exception {
        JsonDocument loaded = bucket.get("21st_amendment_brewery_cafe-21a_ipa");
        sendResource(exchange, "beer", loaded.toString());
    }
}
