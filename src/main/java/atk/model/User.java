package atk.model;

public class User {

    public int id;
    public String name;
    public String email;
    public boolean active;

    public User() {}

    public User(int id, String name, String email, boolean active) {
        this.name = name;
        this.email = email;
        this.active = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", active=" + active +
                '}';
    }
}
